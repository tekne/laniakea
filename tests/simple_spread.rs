use chrono::NaiveDate;
use laniakea::data::IBTick;
use laniakea::market::simple_spread::SpreadBook;
use laniakea::market::*;
use laniakea::strategy::ema::MarketEMAStrategy;
use laniakea::strategy::StrategicTrader;
use laniakea::Portfolio;
use laniakea::TradingPair;
use laniakea::*;
//use smallvec::smallvec;
use tokio::sync::broadcast;
use tokio_stream::{wrappers, StreamExt};

#[tokio::test]
async fn ema_trader() {
    let mut market: Market<SpreadBook> = Market::default();
    let cash = AssetId(0);
    let stock = AssetId(1);
    let trading_pair = TradingPair(cash, stock);
    let portfolio = Portfolio::default();
    let ema_strategy = MarketEMAStrategy::new(trading_pair, 0.2, 10, 10);
    let mut trader = StrategicTrader::with_portfolio(ema_strategy, portfolio, &mut market);
    let date = NaiveDate::from_ymd(2021, 02, 01);

    let ticks = [
        (date.and_hms(16, 00, 00), 8, 9, 4, 4),
        (date.and_hms(16, 00, 01), 4, 5, 1, 2),
        (date.and_hms(16, 00, 02), 2, 10, 2, 10),
        (date.and_hms(16, 00, 03), 10, 13, 8, 9),
        (date.and_hms(16, 00, 04), 9, 10, 8, 2),
    ];
    for &tick in &ticks {
        let tick: IBTick = tick.into();
    }
}
