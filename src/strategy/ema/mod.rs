/*!
A simple EMA-based trader which buys when the price is above the EMA and sells when the price is below the EMA

Only supports a single currency and asset; intended mainly as a simple test baseline for active trading (versus the passive "buy and hold" benchmark).
Supports bars with irregular time differences between them.
*/
use super::*;

/// An exponential moving average aggregator
#[derive(Debug, PartialEq, PartialOrd)]
pub struct EMA {
    /// The timestamp of the previous bar, as an f64
    pub timestamp: Option<Timestamp>,
    /// The current EMA
    pub ema: f64,
    /// The update rate of this EMA
    pub gamma: f64,
}

impl EMA {
    /// Create a new EMA with a given gamma
    pub fn new(gamma: f64) -> EMA {
        EMA {
            timestamp: None,
            ema: f64::NAN,
            gamma,
        }
    }
    /// Update this EMA with a new data point
    pub fn update(&mut self, timestamp: Timestamp, point: f64) {
        if self.ema.is_nan() || self.timestamp.is_none() {
            self.ema = point
        } else if let Some(curr_timestamp) = self.timestamp {
            let dt = (timestamp - curr_timestamp).to_std().unwrap().as_secs_f64();
            let k = self.gamma.powf(dt);
            self.ema *= k;
            self.ema += (1.0 - k) * point;
        }
        self.timestamp = Some(timestamp);
    }
}

/// An exponential moving average trader
///
/// # Strategy
/// Trades a currency for an asset; never *intentionally* takes a negative position in either the currency or the asset.
/// Converts as much currency as possible to asset using a market order when the price is below EMA - below_delta
/// Converts all assets to currency using a market order when price is above EMA + above_delta
/// Saves a 5% currency "bubble" to avoid negative balances.
#[derive(Debug, PartialEq)]
pub struct MarketEMAStrategy {
    /// The current EMA
    pub ema: EMA,
    /// How much below the EMA to buy
    pub below_delta: i64,
    /// How much above the EMA to sell
    pub above_delta: i64,
    /// The current portfolio
    pub portfolio: Portfolio,
    /// Whether there is currently an outstanding order
    pub outstanding: bool,
    /// The number of orders this strategy has made
    pub no_orders: u64,
    /// The pair of assets being traded
    pub trade: TradingPair,
}

impl MarketEMAStrategy {
    /// Create a new unitialized trader
    pub fn new(
        trade: TradingPair,
        gamma: f64,
        below_delta: i64,
        above_delta: i64,
    ) -> MarketEMAStrategy {
        MarketEMAStrategy {
            ema: EMA::new(gamma),
            below_delta,
            above_delta,
            portfolio: Portfolio::default(),
            outstanding: false,
            no_orders: 0,
            trade,
        }
    }
}

impl Strategy for MarketEMAStrategy {
    type Data = (Timestamp, i64);
    fn response(
        &mut self,
        _id: TraderId,
        response: Response,
        _requests: &mut RequestSender,
    ) -> Result<(), RequestError> {
        if let Response::Fill(fill) = &response {
            self.outstanding &= !fill.discharged;
        }
        self.portfolio.handle_response(response);
        Ok(())
    }
    fn update(
        &mut self,
        id: TraderId,
        (timestamp, price): (Timestamp, i64),
        requests: &mut RequestSender,
    ) -> Result<(), RequestError> {
        if !self.outstanding {
            if ((price + self.below_delta) as f64) < self.ema.ema {
                let cash = self.portfolio.get(self.trade.0);
                let to_buy = (cash * 95) / (100 * price);
                let market_order = MarketOrder {
                    trade: self.trade,
                    size: to_buy,
                };
                let order = Order {
                    id: self.no_orders,
                    data: OrderData::Market(market_order),
                };
                self.no_orders += 1;
                requests.send((id, Request::Order(order)))?;
                self.outstanding = true;
            } else if ((price - self.above_delta) as f64) > self.ema.ema {
                let position = self.portfolio.get(self.trade.1);
                let market_order = MarketOrder {
                    trade: self.trade,
                    size: -position,
                };
                let order = Order {
                    id: self.no_orders,
                    data: OrderData::Market(market_order),
                };
                self.no_orders += 1;
                requests.send((id, Request::Order(order)))?;
                self.outstanding = true;
            }
        }
        self.ema.update(timestamp, price as f64);
        Ok(())
    }
}
