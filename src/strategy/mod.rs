/*!
A trading strategy, which converts signals and portfolio status into actionable trades
*/
use crate::*;
use either::Either;
use market::*;
use tokio_stream::{wrappers, Stream, StreamExt};

pub mod ema;
pub mod order;
pub mod sentiment;

/// A trading strategy
pub trait Strategy {
    /// The type of data this strategy accepts
    type Data;
    /// React to a response from the market
    fn response(
        &mut self,
        id: TraderId,
        response: Response,
        requests: &mut RequestSender,
    ) -> Result<(), RequestError>;
    /// React to new data, in the form of a price point
    fn update(
        &mut self,
        id: TraderId,
        data: Self::Data,
        requests: &mut RequestSender,
    ) -> Result<(), RequestError>;
}

/// A trader which uses a single strategy
#[derive(Debug)]
pub struct StrategicTrader<S> {
    /// The strategy in use
    pub strategy: S,
    /// The ID of this trader
    pub id: TraderId,
    /// The request pipe of this trader
    pub requests: RequestSender,
    /// The response pipe of this trader
    pub responses: wrappers::UnboundedReceiverStream<Response>,
}

impl<S> StrategicTrader<S>
where
    S: Strategy,
{
    /// Create a new strategic trader from a strategy, portfolio, and a market
    pub fn with_portfolio<O>(
        strategy: S,
        portfolio: Portfolio,
        market: &mut Market<O>,
    ) -> StrategicTrader<S>
    where
        O: OrderBook,
    {
        let (id, responses) = market.new_trader_with_portfolio(portfolio);
        let requests = market.request_sender.clone();
        StrategicTrader {
            strategy,
            id,
            requests,
            responses: wrappers::UnboundedReceiverStream::new(responses),
        }
    }
    /// Handle up to `n` responses
    pub async fn handle_responses(&mut self, n: u64) -> Result<u64, RequestError> {
        self.handle_events(n, tokio_stream::empty()).await
    }
    /// Handle up to `n` events
    pub async fn handle_events<D>(&mut self, n: u64, data_stream: D) -> Result<u64, RequestError>
    where
        D: Stream<Item = S::Data> + Unpin,
    {
        if n == 0 {
            return Ok(0);
        }
        let mut handled = 0;
        let request_stream = (&mut self.responses).map(Either::Left);
        let data_stream = data_stream.map(Either::Right);
        let mut event_stream = request_stream.merge(data_stream);
        while let Some(event) = event_stream.next().await {
            match event {
                Either::Left(response) => {
                    self.strategy
                        .response(self.id, response, &mut self.requests)?
                }
                Either::Right(data) => self.strategy.update(self.id, data, &mut self.requests)?,
            }
            handled += 1;
            if handled >= n {
                break;
            }
        }
        Ok(handled)
    }
}
