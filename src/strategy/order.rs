/*!
An individual order made by a strategy
*/
use crate::*;
use tokio::sync::mpsc;

/// An order
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Order {
    /// The data behind this order
    pub data: OrderData,
    /// This order's ID. This should be unique per-trader.
    pub id: u64,
}

impl Order {
    /// Get the trading pair to which this order corresponds
    pub fn trade(&self) -> TradingPair {
        match self.data {
            OrderData::Market(market) => market.trade,
            OrderData::Limit(limit) => limit.trade,
        }
    }
}

/// An individual order's data
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
#[non_exhaustive]
pub enum OrderData {
    /// A market order
    Market(MarketOrder),
    /// A limit order
    Limit(LimitOrder),
}

/// A limit order
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct LimitOrder {
    /// The trading pair
    pub trade: TradingPair,
    /// The limit price of this order
    pub price: i64,
    /// The size of this order
    pub size: i64,
}

/// A market order
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct MarketOrder {
    /// The trading pair
    pub trade: TradingPair,
    /// The size of this order
    pub size: i64,
}

/// A request to the market
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum Request {
    /// Make an order
    Order(Order),
    /// Cancel an order
    Cancel(Order),
}

/// A response sender
pub type ResponseSender = mpsc::UnboundedSender<Response>;

/// A response receiver
pub type ResponseReceiver = mpsc::UnboundedReceiver<Response>;

/// A response sender
pub type RequestSender = mpsc::UnboundedSender<(TraderId, Request)>;

/// A response receiver
pub type RequestReceiver = mpsc::UnboundedReceiver<(TraderId, Request)>;

/// An error sending a response
pub type ResponseError = mpsc::error::SendError<Response>;

/// An error sending a request
pub type RequestError = mpsc::error::SendError<(TraderId, Request)>;

/// A fill of an order
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Fill {
    /// The order to which this fill pertains
    pub order: Order,
    /// The transaction which occured
    pub transaction: Transaction,
    /// Whether the order has now been completely discharged
    pub discharged: bool,
}

/// A transaction
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Transaction {
    /// The trading pair
    pub trade: TradingPair,
    /// The price traded at
    pub price: i64,
    /// The size of this order
    pub size: i64,
}

/// A response from the market
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum Response {
    /// An order has been filled.
    Fill(Fill),
    /// An order has been cancelled
    Cancel(Order),
    /// An order was never placed, and hence cannot be cancelled
    NotPlaced(Order),
    /// An order was invalid, and so was never executed
    Rejected(Order, Option<Error>),
}

/// A small vector of responses
pub type ResponseVec = SmallVec<[Response; 1]>;
/// A small vector of trader IDs and responses
pub type TraderResponseVec = SmallVec<[(TraderId, Response); 1]>;

/// An error handling an order
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum Error {
    /// An asset cannot be used as currency to purchase another, *or* the asset is not a currency at all
    WrongAssetCurrency {
        /// The asset being bought
        asset: AssetId,
        /// The (invalid) currency offered
        currency: AssetId,
    },
    /// An asset is not being traded
    NotTraded(AssetId),
    /// An order type is not supported
    OrderTypeNotSupported,
}
