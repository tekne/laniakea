/*!
Forms of market data
*/
use crate::*;
use chrono::NaiveDateTime;
use nom::{
    bytes::complete::{is_not, tag},
    character::complete::{digit1, newline, space0},
    combinator::{map, map_opt, map_res, opt, recognize},
    multi::many0_count,
    sequence::{delimited, pair, preceded, tuple},
    IResult,
};
use serde::Deserialize;

/// An Interactive Brokers candle
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd, Deserialize)]
pub struct IBCandle {
    /// The index of this candle
    #[serde(rename = "")]
    pub ix: u64,
    /// The time of this candle
    pub date: NaiveDateTime,
    /// The opening price of this candle in microdollars
    pub open: i64,
    /// The high price of this candle in microdollars
    pub high: i64,
    /// The low price of this candle in microdollars
    pub low: i64,
    /// The closing price of this candle in microdollars
    pub close: i64,
    /// The volume of this candle
    pub volume: u64,
    /// The average price of this candle in microdollars
    pub average: i64,
    /// The number of trades of this candle
    #[serde(rename = "popCount")]
    pub bar_count: u64,
}

impl IBCandle {
    /// Validate this IB candle, performing some basic sanity checks on the data inside
    ///
    /// Return whether the check passed. Currently only checks whether high/low are higher/lower than the other prices.
    pub fn validate(&self) -> bool {
        self.high >= self.open
            && self.high >= self.low
            && self.high >= self.close
            && self.high >= self.average
            && self.low <= self.open
            && self.low <= self.high
            && self.low <= self.close
            && self.low <= self.average
    }
    /// Transform this candle into a pessimistic spread, with the high as the ask and the low as the bid
    pub fn pessimistic(&self) -> Spread {
        Spread {
            bid: self.low,
            ask: self.high,
        }
    }
    /// Convert this candle into a pessimistic spread with a UTC timestamp
    pub fn pessimistic_utc(self) -> (Timestamp, Spread) {
        (Timestamp::from_utc(self.date, Utc), self.pessimistic())
    }
    /// Get this timestamp as a UTC timestamp
    pub fn utc_datetime(self) -> Timestamp {
        Timestamp::from_utc(self.date, Utc)
    }
}

/// An Interactive Brokers tick
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct IBTick {
    /// The time of this tick
    pub datetime: NaiveDateTime,
    /// The opening price of this tick in microdollars
    pub open: i64,
    /// The closing price of this tick in microdollars
    pub close: i64,
    /// The high price of this tick in microdollars
    pub high: i64,
    /// The low price of this tick in microdollars
    pub low: i64,
}

impl IBTick {
    /// Utility constructor for a tick
    pub fn ohlc(datetime: NaiveDateTime, open: i64, close: i64, high: i64, low: i64) -> IBTick {
        IBTick {
            datetime,
            open,
            close,
            high,
            low,
        }
    }
    /// Validate this IB tick, performing some basic sanity checks on the data inside
    ///
    /// Return whether the check passed.
    pub fn validate(&self) -> bool {
        self.high >= self.open
            && self.high >= self.low
            && self.high >= self.close
            && self.low <= self.open
            && self.low <= self.high
            && self.low <= self.close
    }
    /// Transform this tick into a pessimistic spread, with the high as the ask and the low as the bid
    pub fn pessimistic(self) -> Spread {
        Spread {
            bid: self.low,
            ask: self.high,
        }
    }
    /// Convert this tick into a closing price point, interpreting the date as Utc
    pub fn close_utc(self) -> (Timestamp, i64) {
        (Timestamp::from_utc(self.datetime, Utc), self.close)
    }
    /// Convert this tick into a pessimistic spread with a UTC timestamp
    pub fn pessimistic_utc(self) -> (Timestamp, Spread) {
        (Timestamp::from_utc(self.datetime, Utc), self.pessimistic())
    }
    /// Get this timestamp as a UTC timestamp
    pub fn utc_datetime(self) -> Timestamp {
        Timestamp::from_utc(self.datetime, Utc)
    }
}

impl From<(NaiveDateTime, i64, i64, i64, i64)> for IBTick {
    fn from((datetime, open, close, high, low): (NaiveDateTime, i64, i64, i64, i64)) -> IBTick {
        IBTick {
            datetime,
            open,
            close,
            high,
            low,
        }
    }
}

/// Parse a signed floating point as microdollars
pub fn microdollars(input: &str) -> IResult<&str, i64> {
    map_opt(
        tuple((opt(tag("-")), digit1, opt(pair(tag("."), digit1)))),
        |(negate, digits, decimal_digits)| {
            let digits = i64::from_str_radix(digits, 10).ok()?;
            let (decimal_digits, no_decimal_digits) = decimal_digits
                .map(|(_, digits)| i64::from_str_radix(digits, 10).map(|x| (x, digits.len())))
                .unwrap_or(Ok((0, 1)))
                .ok()?;
            let decimal_factor = 1_000_000i64
                .checked_div(10i64.checked_pow(no_decimal_digits as u32)?)?
                .max(1);
            let sign = if negate.is_some() { -1 } else { 1 };
            digits
                .checked_mul(1_000_000)?
                .checked_add(decimal_digits.checked_mul(decimal_factor)?)?
                .checked_mul(sign)
        },
    )(input)
}

/// Helper to parse a u64
pub fn ix(input: &str) -> IResult<&str, u64> {
    map_res(digit1, str::parse)(input)
}

/// Helper to parse a CSV separator
pub fn csv_sep(input: &str) -> IResult<&str, ()> {
    map(delimited(space0, tag(","), space0), |_| ())(input)
}

/// Parse a CSV line, returning the number of fields
pub fn line(input: &str) -> IResult<&str, usize> {
    delimited(
        space0,
        preceded(
            opt(is_not(",\n\r")),
            map(many0_count(preceded(tag(","), opt(is_not(",\n\r")))), |x| {
                x + 1
            }),
        ),
        newline,
    )(input)
}

/// Parse an IB format date
pub fn ib_date(input: &str) -> IResult<&str, NaiveDateTime> {
    map_res(
        recognize(tuple((
            digit1,
            tag("-"),
            digit1,
            tag("-"),
            digit1,
            space0,
            digit1,
            tag(":"),
            digit1,
            tag(":"),
            digit1,
        ))),
        |potential_date| NaiveDateTime::parse_from_str(potential_date, "%Y-%m-%d %H:%M:%S"),
    )(input)
}

/// Parse a single IB candle
pub fn ib_candle(input: &str) -> IResult<&str, IBCandle> {
    map(
        tuple((
            preceded(space0, ix),
            preceded(csv_sep, ib_date),
            preceded(csv_sep, microdollars),
            preceded(csv_sep, microdollars),
            preceded(csv_sep, microdollars),
            preceded(csv_sep, microdollars),
            preceded(csv_sep, ix),
            preceded(csv_sep, microdollars),
            preceded(csv_sep, ix),
            opt(preceded(space0, newline)),
        )),
        |(ix, date, open, high, low, close, volume, average, bar_count, _)| IBCandle {
            ix,
            date,
            open,
            high,
            low,
            close,
            volume,
            average,
            bar_count,
        },
    )(input)
}

/// Parse a single IB tick
pub fn ib_tick(input: &str) -> IResult<&str, IBTick> {
    map(
        tuple((
            preceded(space0, ib_date),
            preceded(csv_sep, microdollars),
            preceded(csv_sep, microdollars),
            preceded(csv_sep, microdollars),
            preceded(csv_sep, microdollars),
            opt(preceded(space0, newline)),
        )),
        |(datetime, open, high, low, close, _)| IBTick {
            datetime,
            open,
            high,
            low,
            close,
        },
    )(input)
}

#[cfg(test)]
mod test {
    use super::*;
    use chrono::NaiveDate;
    #[test]
    fn microdollar_parsing() {
        let examples = [
            ("100.21", Some(("", 100_21_0000))),
            ("1256.25321, hello", Some((", hello", 1256_25321_0))),
            ("abc", None),
        ];
        for &(input, output) in &examples {
            assert_eq!(microdollars(input).ok(), output)
        }
    }
    #[test]
    fn ib_date_parsing() {
        let examples = [
            (
                "2015-09-05 23:56:04",
                Some(("", NaiveDate::from_ymd(2015, 9, 5).and_hms(23, 56, 4))),
            ),
            ("abc", None),
        ];
        for &(input, output) in &examples {
            assert_eq!(ib_date(input).ok(), output)
        }
    }
    #[test]
    fn load_ib_candles() {
        let data = "\
        ,date,open,high,low,close,volume,average,barCount
        0,2020-12-31 09:30:00,29.56,29.70,29.30,29.65,250,29.55,40
        1,2020-12-31 09:31:00,29.65,29.75,29.00,29.10,270,29.30,53";
        let (ib_data, fields) = line(data).unwrap();
        assert_eq!(fields, 9);
        let (_rest, ib_fields) = line(ib_data).unwrap();
        assert_eq!(ib_fields, 9);

        let date = NaiveDate::from_ymd(2020, 12, 31);
        let candles = [
            IBCandle {
                ix: 0,
                date: date.and_hms(9, 30, 00),
                open: 29_56_0000,
                high: 29_70_0000,
                low: 29_30_0000,
                close: 29_65_0000,
                volume: 250,
                average: 29_55_0000,
                bar_count: 40,
            },
            IBCandle {
                ix: 1,
                date: date.and_hms(9, 31, 00),
                open: 29_65_0000,
                high: 29_75_0000,
                low: 29_00_0000,
                close: 29_10_0000,
                volume: 270,
                average: 29_30_0000,
                bar_count: 53,
            },
        ];

        let (rest, parse_0) = ib_candle(ib_data).unwrap();
        assert_eq!(candles[0], parse_0);
        let (_rest, parse_1) = ib_candle(rest).unwrap();
        assert_eq!(candles[1], parse_1);
    }
}
