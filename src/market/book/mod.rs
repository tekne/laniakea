/*!
A simulated order book, containing price information, which handles orders
*/
use super::*;

pub mod simple_spread;

/// A simulated order book
pub trait OrderBook {
    /// The type of data updates this order book requires
    type Data: Clone;
    /// Update the state of the order book with some data
    fn update(&mut self, data: Self::Data, traders: &mut TraderSet);
    /// Handle a request to the order book. Return the request on failure.
    fn request(
        &mut self,
        trader: TraderId,
        request: Request,
        traders: &mut TraderSet,
    ) -> Option<()>;
    /// Value a portfolio in an asset
    fn value(&self, _portfolio: Portfolio, _asset: AssetId) -> Result<i64, Option<Error>> {
        Err(None)
    }
}

/// The empty order book, which consumes no data and always fails to process any order
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct EmptyBook;

impl OrderBook for EmptyBook {
    type Data = ();
    fn update(&mut self, _data: (), _traders: &mut TraderSet) {}
    fn request(
        &mut self,
        trader: TraderId,
        request: Request,
        traders: &mut TraderSet,
    ) -> Option<()> {
        let trader = traders.get(trader)?;
        let response = match request {
            Request::Order(order) => Response::Rejected(order, None),
            Request::Cancel(order) => Response::NotPlaced(order),
        };
        trader.notify(response).ok()?;
        Some(())
    }
}
