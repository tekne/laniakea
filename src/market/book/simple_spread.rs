/*!
Simple order book for bar data
*/

use super::*;
use crate::asset::*;

/// Simple spread-based order book, assuming infinite supply at the ask price and infinite demand at the bid price for the length of a given bar.
#[derive(Debug, Clone, Eq, PartialEq, Default)]
pub struct SpreadBook {
    /// Current trading pairs
    trading_pairs: BinAssetMap<SingleSpreadBook>,
}

/// A spread-based order book for a single stock
#[derive(Debug, Clone, Eq, PartialEq, Default)]
pub struct SingleSpreadBook {
    /// The most recent bar for this trading pair
    bar: Option<(Timestamp, Spread)>,
    /// The orders for this stock
    orders: Vec<(TraderId, Order)>,
}

impl SingleSpreadBook {
    /// Try to fill a limit order at a given bar; return `true` if the order was filled
    pub fn fill_limit_order(
        bar: Spread,
        trader: TraderId,
        order: &LimitOrder,
        id: u64,
        traders: &mut TraderSet,
    ) -> bool {
        let trader = if let Some(trader) = traders.get_mut(trader) {
            trader
        } else {
            return true;
        };
        //TODO: "optimistic" mode using previous price?
        let price = if order.size < 0 {
            if order.price > bar.bid {
                return false;
            }
            bar.bid
        } else {
            if order.price < bar.ask {
                return false;
            }
            bar.ask
        };
        let fill = Fill {
            order: Order {
                data: OrderData::Limit(*order),
                id,
            },
            transaction: Transaction {
                trade: order.trade,
                price,
                size: order.size,
            },
            discharged: true,
        };
        let _ = trader.respond(Response::Fill(fill));
        true
    }
    /// Try to fill a market order at a given bar; return `true` if the order was filled
    pub fn fill_market_order(
        bar: Spread,
        trader: TraderId,
        order: &MarketOrder,
        id: u64,
        traders: &mut TraderSet,
    ) -> bool {
        let trader = if let Some(trader) = traders.get_mut(trader) {
            trader
        } else {
            return true;
        };
        //TODO: "optimistic" mode using previous price?
        let price = if order.size < 0 { bar.bid } else { bar.ask };
        let fill = Fill {
            order: Order {
                data: OrderData::Market(*order),
                id,
            },
            transaction: Transaction {
                trade: order.trade,
                price,
                size: order.size,
            },
            discharged: true,
        };
        let _ = trader.respond(Response::Fill(fill));
        true
    }
    /// Handle an invalid order given by a particular trader
    pub fn invalid_order(
        _bar: Spread,
        trader: TraderId,
        order: &Order,
        traders: &mut TraderSet,
    ) -> bool {
        let response = Response::Rejected(*order, Some(Error::OrderTypeNotSupported));
        if let Some(trader) = traders.get_mut(trader) {
            let _ = trader.respond(response);
        }
        true
    }
    /// Try to fill an order at a given bar; return `true` if the order was filled
    pub fn fill_order(bar: Spread, trader: TraderId, order: &Order, traders: &mut TraderSet) -> bool {
        let id = order.id;
        match &order.data {
            OrderData::Limit(order) => Self::fill_limit_order(bar, trader, order, id, traders),
            OrderData::Market(order) => Self::fill_market_order(bar, trader, order, id, traders),
        }
    }
    /// Attempt to fill the orders in this book. Return the number of orders filled.
    pub fn fill_orders(&mut self, traders: &mut TraderSet) -> usize {
        let old_len = self.orders.len();
        if let Some((_timestamp, bar)) = self.bar {
            self.orders
                .retain(|(trader, order)| !Self::fill_order(bar, *trader, order, traders));
        }
        old_len - self.orders.len()
    }
}

impl SpreadBook {
    /// Handle the arrival of a bar for a trading pair
    pub fn handle_bar(
        &mut self,
        timestamp: Timestamp,
        pair: TradingPair,
        bar: Spread,
        traders: &mut TraderSet,
    ) {
        // Find the order book for the given trading pair
        let order_book = self.trading_pairs.entry(pair).or_default();
        // Update the order book
        if let Some((old_timestamp, _)) = &mut order_book.bar {
            if *old_timestamp >= timestamp {
                // Outdated data, ignore
                return;
            }
        }
        order_book.bar = Some((timestamp, bar));
        // Fill orders in the book
        order_book.fill_orders(traders);
    }
    /// Place an order an order; return `true` if the order was filled
    pub fn place_order(&mut self, trader: TraderId, order: Order, traders: &mut TraderSet) {
        let pair = order.trade();
        let order_book = self.trading_pairs.entry(pair).or_default();
        if let Some((_, bar)) = order_book.bar {
            if SingleSpreadBook::fill_order(bar, trader, &order, traders) {
                // The order has been filled, so there's nothing left to do
                return;
            }
        }
        // Push the order to the order book's queue
        order_book.orders.push((trader, order));
    }
    /// Get the most recent bar for a trading pair, if any
    pub fn bar(&self, pair: TradingPair) -> Option<(Timestamp, Spread)> {
        self.trading_pairs.get(&pair).map(|book| book.bar).flatten()
    }
}

/// A set of spreads
pub type SpreadSet = SmallVec<[(Timestamp, TradingPair, Spread); 1]>;

impl OrderBook for SpreadBook {
    type Data = SpreadSet;
    fn update(&mut self, data: SpreadSet, traders: &mut TraderSet) {
        for &(timestamp, pair, bar) in data.iter() {
            self.handle_bar(timestamp, pair, bar, traders)
        }
    }
    fn request(
        &mut self,
        trader: TraderId,
        request: Request,
        traders: &mut TraderSet,
    ) -> Option<()> {
        match request {
            Request::Order(order) => {
                self.place_order(trader, order, traders);
                Some(())
            }
            Request::Cancel(order) => panic!(
                "Cancelling orders is not yet supported! Tried to cancel {:#?}",
                order
            ),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use smallvec::smallvec;
    use std::task::Poll;
    #[test]
    fn basic_double_stock_trading() {
        // Get a noop waker
        let waker = futures::task::noop_waker();
        let mut context = std::task::Context::from_waker(&waker);

        // Initialize the market and the test trader
        let mut market: Market<SpreadBook> = Market::default();
        let (trader, mut recv) = market.new_trader();

        // Initialize stocks and trading pairs
        const CASH: AssetId = AssetId(0);
        const STOCK: AssetId = AssetId(1);
        const STONK: AssetId = AssetId(2);
        const LONG_STOCK: TradingPair = TradingPair(CASH, STOCK);
        const LONG_STONK: TradingPair = TradingPair(CASH, STONK);

        // Check the market has no data
        assert_eq!(market.order_book.bar(LONG_STOCK), None);
        assert_eq!(market.order_book.bar(LONG_STONK), None);

        // Place a market order, which *should not* get filled
        let market_order = Order {
            data: OrderData::Market(MarketOrder {
                trade: LONG_STONK,
                size: 100,
            }),
            id: 0,
        };
        market
            .request(trader, Request::Order(market_order))
            .unwrap();
        // Place two limit orders, which *should not* get filled
        let limit_order_1 = Order {
            data: OrderData::Limit(LimitOrder {
                trade: LONG_STOCK,
                size: 100,
                price: 10,
            }),
            id: 1,
        };
        market
            .request(trader, Request::Order(limit_order_1))
            .unwrap();
        let limit_order_2 = Order {
            data: OrderData::Limit(LimitOrder {
                trade: LONG_STOCK,
                size: -100,
                price: 12,
            }),
            id: 2,
        };
        market
            .request(trader, Request::Order(limit_order_2))
            .unwrap();

        // Check the market still has no data
        assert_eq!(market.order_book.bar(LONG_STOCK), None);
        assert_eq!(market.order_book.bar(LONG_STONK), None);

        // Check no trades have been completed
        assert_eq!(recv.poll_recv(&mut context), Poll::Pending);

        // Update the market for STOCK and STONK: this should cause *some*, but not *all*, of the transactions to go through
        let timestamp_1 = DateTime::parse_from_rfc2822("Wed, 18 Feb 2015 23:16:00 GMT")
            .unwrap()
            .into();
        let bar_1 = Spread {
            bid: 10,
            ask: 12,
        };
        let bar_2 = Spread {
            bid: 22,
            ask: 27,
        };
        let data = smallvec![
            (timestamp_1, LONG_STOCK, bar_1),
            (timestamp_1, LONG_STONK, bar_2)
        ];
        market.update(data);

        // Check the bars have actually been updated
        assert_eq!(
            market.order_book.bar(LONG_STOCK),
            Some((timestamp_1, bar_1))
        );
        assert_eq!(
            market.order_book.bar(LONG_STONK),
            Some((timestamp_1, bar_2))
        );

        // Check one trade has been filled, and that this is reflected in trader portfolios
        assert_eq!(
            market.portfolio(trader).unwrap().get(CASH),
            -100 * bar_2.ask
        );
        assert_eq!(market.portfolio(trader).unwrap().get(STOCK), 0);
        assert_eq!(market.portfolio(trader).unwrap().get(STONK), 100);
        let fill = Fill {
            order: market_order,
            transaction: Transaction {
                trade: LONG_STONK,
                price: 27,
                size: 100,
            },
            discharged: true,
        };
        assert_eq!(
            recv.poll_recv(&mut context),
            Poll::Ready(Some(Response::Fill(fill)))
        );
        assert_eq!(recv.poll_recv(&mut context), Poll::Pending);

        // Update the market, but just for STOCK: this should cause the limit order to go through
        let timestamp_2 = DateTime::parse_from_rfc2822("Wed, 18 Feb 2015 23:17:00 GMT")
            .unwrap()
            .into();
        let bar_3 = Spread {
            bid: 13,
            ask: 15,
        };
        let data = smallvec![(timestamp_2, LONG_STOCK, bar_3),];
        market.update(data);
        assert_eq!(
            market.order_book.bar(LONG_STOCK),
            Some((timestamp_2, bar_3))
        );
        assert_eq!(
            market.order_book.bar(LONG_STONK),
            Some((timestamp_1, bar_2))
        );

        // Check the limit order for STOCK, but not for STONK, has been filled
        assert_eq!(
            market.portfolio(trader).unwrap().get(CASH),
            -100 * bar_2.ask + 100 * bar_3.bid
        );
        assert_eq!(market.portfolio(trader).unwrap().get(STOCK), -100);
        assert_eq!(market.portfolio(trader).unwrap().get(STONK), 100);
        let fill = Fill {
            order: limit_order_2,
            transaction: Transaction {
                trade: LONG_STOCK,
                price: 13,
                size: -100,
            },
            discharged: true,
        };
        assert_eq!(
            recv.poll_recv(&mut context),
            Poll::Ready(Some(Response::Fill(fill)))
        );
        assert_eq!(recv.poll_recv(&mut context), Poll::Pending);
    }
}
