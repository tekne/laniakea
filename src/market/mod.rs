/*!
A simulated market for strategies to compete in
*/
use crate::*;

pub mod book;
pub use book::*;

use crate::portfolio::Portfolio;
use crate::strategy::order::*;
use either::Either;
use generational_arena::{Arena, Index};
use tokio::sync::mpsc;
use tokio_stream::{wrappers::UnboundedReceiverStream, Stream, StreamExt};

/// A trader ID
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct TraderId(Index);

/// A set of traders
#[derive(Debug, Default)]
pub struct TraderSet(Arena<Trader>);

impl TraderSet {
    /// Get the trader corresponding to an ID in this set, if any
    pub fn get(&self, id: TraderId) -> Option<&Trader> {
        self.0.get(id.0)
    }
    /// Mutably get the trader corresponding to an ID in this set, if any
    pub fn get_mut(&mut self, id: TraderId) -> Option<&mut Trader> {
        self.0.get_mut(id.0)
    }
    /// Insert a new trader into this set, yielding it's ID
    pub fn insert(&mut self, trader: Trader) -> TraderId {
        TraderId(self.0.insert(trader))
    }
}

/// A simulated market for strategies to compete in
#[derive(Debug)]
pub struct Market<O> {
    /// The order book of this market
    pub order_book: O,
    /// The receiver for this market's requests
    pub requests: UnboundedReceiverStream<(TraderId, Request)>,
    /// A sender for this market's requests
    pub request_sender: RequestSender,
    /// The traders in this market
    pub traders: TraderSet,
}

impl<O: OrderBook> Market<O> {
    /// Create a new market from a given order book, with a given channel capacity
    pub fn new(order_book: O) -> Market<O> {
        let (request_sender, requests) = mpsc::unbounded_channel();
        Market {
            order_book,
            requests: UnboundedReceiverStream::new(requests),
            request_sender,
            traders: TraderSet::default(),
        }
    }
    /// Register a new trader with an empty portfolio
    pub fn new_trader(&mut self) -> (TraderId, ResponseReceiver) {
        self.new_trader_with_portfolio(Portfolio::default())
    }
    /// Register a new trader, returning a trader ID and a response source
    pub fn new_trader_with_portfolio(
        &mut self,
        portfolio: Portfolio,
    ) -> (TraderId, ResponseReceiver) {
        let (trader, recv) = Trader::with_portfolio(portfolio);
        let id = self.traders.insert(trader);
        (id, recv)
    }
    /// Register a new trader and after immediately putting in a list of requests
    pub fn new_trader_with_requests(
        &mut self,
        requests: &[Request],
    ) -> (TraderId, ResponseReceiver) {
        self.new_trader_with_requests_and_portfolio(Portfolio::default(), requests)
    }
    /// Register a new trader with a given portfolio, and immediately put in a list of requests
    pub fn new_trader_with_requests_and_portfolio(
        &mut self,
        portfolio: Portfolio,
        requests: &[Request],
    ) -> (TraderId, ResponseReceiver) {
        let (trader, responses) = self.new_trader_with_portfolio(portfolio);
        for &request in requests {
            self.request(trader, request);
        }
        (trader, responses)
    }
    /// Make a request
    pub fn request(&mut self, id: TraderId, request: Request) -> Option<()> {
        self.order_book.request(id, request, &mut self.traders)
    }
    /// Update this market with new data
    pub fn update(&mut self, data: O::Data) {
        self.order_book.update(data, &mut self.traders)
    }
    /// Get the trader corresponding to an ID, if any
    pub fn trader(&self, id: TraderId) -> Option<&Trader> {
        self.traders.get(id)
    }
    /// Mutably get the trader corresponding to an ID, if any
    pub fn trader_mut(&mut self, id: TraderId) -> Option<&mut Trader> {
        self.traders.get_mut(id)
    }
    /// Get the portfolio of a trader corresponding to an ID, if any
    pub fn portfolio(&self, id: TraderId) -> Option<&Portfolio> {
        self.traders.get(id).map(|trader| &trader.portfolio)
    }
    /// Handle up to `n` updates
    pub async fn handle_updates(&mut self, n: u64) -> Result<u64, u64> {
        self.handle_events(n, tokio_stream::empty()).await
    }
    /// Handle up to `n` events from a data stream
    pub async fn handle_events<D>(&mut self, n: u64, data_stream: D) -> Result<u64, u64>
    where
        D: Stream<Item = O::Data> + Unpin,
    {
        if n == 0 {
            return Ok(0);
        }
        let mut handled = 0;
        let request_stream = (&mut self.requests).map(Either::Left);
        let data_stream = data_stream.map(Either::Right);
        let mut event_stream = request_stream.merge(data_stream);
        while let Some(event) = event_stream.next().await {
            match event {
                Either::Left((trader, request)) => self
                    .order_book
                    .request(trader, request, &mut self.traders)
                    .ok_or(handled)?,
                Either::Right(data) => self.order_book.update(data, &mut self.traders),
            }
            handled += 1;
            if handled >= n {
                break;
            }
        }
        Ok(handled)
    }
}

impl<O> Default for Market<O>
where
    O: OrderBook + Default,
{
    fn default() -> Market<O> {
        Market::new(O::default())
    }
}

/// A trader
#[derive(Debug)]
pub struct Trader {
    /// This trader's response sink
    pub responses: ResponseSender,
    /// This trader's portfolio
    pub portfolio: Portfolio,
}

impl Trader {
    /// Create a new, default trader
    pub fn new() -> (Trader, ResponseReceiver) {
        Self::with_portfolio(Portfolio::default())
    }
    /// Create a new trader with a given portfolio and buffer size
    pub fn with_portfolio(portfolio: Portfolio) -> (Trader, ResponseReceiver) {
        let (responses, recv) = mpsc::unbounded_channel();
        let trader = Trader {
            responses,
            portfolio,
        };
        (trader, recv)
    }
    /// Notify this trader of a response. Does *not* affect the trader's portfolio!
    pub fn notify(&self, response: Response) -> Result<(), ResponseError> {
        self.responses.send(response)
    }
    /// Respond to this trader. Modifies the portfolio appropriately
    pub fn respond(&mut self, response: Response) -> Result<(), ResponseError> {
        self.portfolio.handle_response(response);
        self.responses.send(response)
    }
}
