/*!
A set of positions in financial instruments
*/

//TODO: optimize for integer keys
use crate::asset::*;
use crate::strategy::order::*;
use std::collections::hash_map::Entry;

/// A set of positions in financial instruments
#[derive(Debug, Clone, Eq, PartialEq, Default)]
pub struct Portfolio {
    positions: AssetMap<i64>,
}

impl Portfolio {
    /// Create a new, empty portfolio
    pub fn new() -> Portfolio {
        Portfolio::default()
    }
    /// Get the position in a given security
    pub fn get(&self, security: AssetId) -> i64 {
        self.positions.get(&security).copied().unwrap_or(0)
    }
    /// Set the position in a given security, returning the previous position
    pub fn set(&mut self, security: AssetId, pos: i64) -> i64 {
        self.positions.insert(security, pos).unwrap_or(0)
    }
    /// Add to the position in a given security, returning the previous position
    pub fn add(&mut self, security: AssetId, pos: i64) -> i64 {
        match self.positions.entry(security) {
            Entry::Vacant(v) => {
                v.insert(pos);
                0
            }
            Entry::Occupied(mut o) => {
                let value = o.get_mut();
                let old = *value;
                *value += pos;
                old
            }
        }
    }
    /// Compute the value of a portfolio
    pub fn value<P: FnMut(AssetId) -> i64>(&self, mut prices: P) -> i64 {
        let mut value = 0;
        for (&security, &position) in self.positions.iter() {
            value += prices(security) * position;
        }
        value
    }
    /// Handle a response to an order
    pub fn handle_response(&mut self, response: Response) {
        match response {
            Response::Fill(Fill { transaction, .. }) => {
                let TradingPair(currency, asset) = transaction.trade;
                self.add(asset, transaction.size);
                self.add(currency, -transaction.size * transaction.price);
            }
            _ => {}
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn basic_portfolio_functionality() {
        let mut portfolio = Portfolio::new();
        let id_prices = |security: AssetId| security.0 as i64;
        assert_eq!(portfolio.value(id_prices), 0);
        assert_eq!(portfolio.add(AssetId(1), 25), 0);
        assert_eq!(portfolio.add(AssetId(2), 30), 0);
        assert_eq!(portfolio.value(id_prices), 25 + 2 * 30);
        assert_eq!(portfolio.add(AssetId(1), -5), 25);
        assert_eq!(portfolio.value(id_prices), 20 + 2 * 30);
        assert_eq!(portfolio.value(|_| 0), 0);
    }
}
