/*!
A simple simulated trading engine for backtesting strategies
*/

#![forbid(missing_docs, missing_debug_implementations)]

use chrono::{DateTime, Utc};
use smallvec::SmallVec;

pub mod asset;
pub mod data;
pub mod market;
pub mod portfolio;
pub mod strategy;

pub use asset::*;
pub use market::TraderId;
pub use portfolio::*;
pub use strategy::order::*;

/// A timestamp
pub type Timestamp = DateTime<Utc>;

/// A bid-ask spread
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Spread {
    /// The bid
    pub bid: i64,
    /// The ask
    pub ask: i64,
}
