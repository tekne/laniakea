/*!
An individual asset supported by the engine
*/
use hashers::null::PassThroughHasher;
use std::collections::HashMap;
use std::hash::BuildHasherDefault;

/// The ID for an individual asset
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct AssetId(pub u64);

/// A trading pair of two assets
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct TradingPair(pub AssetId, pub AssetId);

/// An optimized map from assets to a given type
pub type AssetMap<T> = HashMap<AssetId, T, BuildHasherDefault<PassThroughHasher>>;

/// An optimized map from pairs of assets to a given type
pub type BinAssetMap<T> = HashMap<TradingPair, T, fxhash::FxBuildHasher>;
